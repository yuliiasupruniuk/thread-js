import { createAsyncThunk } from '@reduxjs/toolkit';
import { HttpError } from 'exceptions/exceptions';
import { HttpCode, StorageKey, ExceptionMessage } from 'common/enums/enums';

import { ActionType } from './common';

const login = createAsyncThunk(
  ActionType.LOG_IN,
  async (request, { extra: { services } }) => {
    const { user, token } = await services.auth.login(request);

    services.storage.setItem(StorageKey.TOKEN, token);

    return user;
  }
);

const register = createAsyncThunk(
  ActionType.REGISTER,
  async (request, { extra: { services } }) => {
    const { user, token } = await services.auth.registration(request);

    services.storage.setItem(StorageKey.TOKEN, token);

    return user;
  }
);

const logout = createAsyncThunk(
  ActionType.LOG_OUT,
  (_request, { extra: { services } }) => {
    services.storage.removeItem(StorageKey.TOKEN);

    return null;
  }
);

const loadCurrentUser = createAsyncThunk(
  ActionType.LOG_IN,
  async (_request, { dispatch, rejectWithValue, extra: { services } }) => {
    try {
      return await services.auth.getCurrentUser();
    } catch (err) {
      const isHttpError = err instanceof HttpError;

      if (isHttpError && err.status === HttpCode.UNAUTHORIZED) {
        dispatch(logout());
      }

      return rejectWithValue(err?.message ?? ExceptionMessage.UNKNOWN_ERROR);
    }
  }
);

const updateUsername = createAsyncThunk(
  ActionType.EDIT_USER,
  async (userInfo, { extra: { services } }) => {
    const { id, username } = userInfo;
    const updatedUser = await services.auth.updateUser(id, username);

    return updatedUser;
  }
);

const getUsers = createAsyncThunk(
  ActionType.GET_ALL,
  async (_request, { extra: { services } }) => {
    const users = await services.auth.getUsers();

    return users;
  }
);

export { login, register, logout, loadCurrentUser, updateUsername, getUsers };
