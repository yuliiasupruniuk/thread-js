const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  REACT: 'thread/react',
  LOAD_REACTION: 'thread/load-react',
  COMMENT: 'thread/comment',
  DELETE: 'thread/delete-post',
  EDIT_POST: 'thread/edit-post',
  EDIT_COMMENT: 'thread/edit-comment'
};

export { ActionType };

