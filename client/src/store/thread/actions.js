import { createAsyncThunk } from '@reduxjs/toolkit';
import { ActionType } from './common';

const loadPosts = createAsyncThunk(
  ActionType.SET_ALL_POSTS,
  async (filters, { extra: { services } }) => {
    const posts = await services.post.getAllPosts(filters);
    return { posts };
  }
);

const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filters, { getState, extra: { services } }) => {
    const {
      posts: { posts }
    } = getState();

    const loadedPosts = await services.post.getAllPosts(filters);
    const filteredPosts = loadedPosts.filter(
      post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );

    return { posts: filteredPosts };
  }
);

const getLikedOrDislikedUsers = createAsyncThunk(
  ActionType.LOAD_USERS,
  async (postReactions, { extra: { services } }) => {
    const { id, reaction } = postReactions;
    const users = await services.post.getLikedOrDislikedUsers(id, reaction);
    return users;
  }
);

const deletePost = createAsyncThunk(
  ActionType.DELETE,
  async (postId, { extra: { services } }) => {
    const result = await services.post.deletePost(postId);
    return result;
  }
);

const applyPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (postId, { extra: { services } }) => {
    const post = await services.post.getPost(postId);
    return { post };
  }
);

const createPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (post, { extra: { services } }) => {
    const { id } = await services.post.addPost(post);
    const newPost = await services.post.getPost(id);

    return { post: newPost };
  }
);

const editPost = createAsyncThunk(
  ActionType.EDIT_POST,
  async (updatedPost, { extra: { services } }) => {
    const { postId, userId, body } = updatedPost;
    const editedPost = await services.post.editPost(postId, userId, body);

    return editedPost;
  }
);

const toggleExpandedPost = createAsyncThunk(
  ActionType.SET_EXPANDED_POST,
  async (postId, { extra: { services } }) => {
    const post = postId ? await services.post.getPost(postId) : undefined;
    return { post };
  }
);

const loadReaction = async (postId, userId, services) => {
  const info = await services.post.getPostReaction(postId, userId);
  return info.isLike;
};

const likePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { profile } = getState();
    const isLike = await loadReaction(postId, profile.user.id, services);

    let diff = 0;
    if (isLike !== false) {
      const { id } = await services.post.likePost(postId);
      diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed
    }

    const mapLikes = post => ({
      ...post,
      likeCount: Number(post.likeCount) + diff // diff is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();

    const updated = posts.map(post => (
      post.id !== postId ? post : mapLikes(post)
    ));
    const updatedExpandedPost = expandedPost?.id === postId
      ? mapLikes(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const dislikePost = createAsyncThunk(
  ActionType.REACT,
  async (postId, { getState, extra: { services } }) => {
    const { profile } = getState();
    const isLike = await loadReaction(postId, profile.user.id, services);

    let diff = 0;
    if (isLike !== true) {
      const { id } = await services.post.dislikePost(postId);
      diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed
    }

    const mapDislikes = post => ({
      ...post,
      dislikeCount: Number(post.dislikeCount) + diff
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== postId ? post : mapDislikes(post)
    ));
    const updatedExpandedPost = expandedPost?.id === postId
      ? mapDislikes(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const addComment = createAsyncThunk(
  ActionType.COMMENT,
  async (request, { getState, extra: { services } }) => {
    const { id } = await services.comment.addComment(request);
    const comment = await services.comment.getComment(id);

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) + 1,
      comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
      posts: { posts, expandedPost }
    } = getState();
    const updated = posts.map(post => (
      post.id !== comment.postId ? post : mapComments(post)
    ));

    const updatedExpandedPost = expandedPost?.id === comment.postId
      ? mapComments(expandedPost)
      : undefined;

    return { posts: updated, expandedPost: updatedExpandedPost };
  }
);

const editComment = createAsyncThunk(
  ActionType.EDIT_COMMENT,
  async (updatedComment, { extra: { services } }) => {
    const { id, newBody } = updatedComment;
    const editedComment = await services.comment.editComment(id, newBody);

    return editedComment;
  }
);

const deleteComment = createAsyncThunk(
  ActionType.DELETE,
  async (commentId, { extra: { services } }) => {
    const result = await services.comment.deleteComment(commentId);
    return result;
  }
);

export {
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  toggleExpandedPost,
  likePost,
  editPost,
  dislikePost,
  addComment,
  deletePost,
  getLikedOrDislikedUsers,
  editComment,
  deleteComment
};
