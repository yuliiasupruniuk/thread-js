import PropTypes from 'prop-types';
import { useState } from 'react';

import { getFromNowTime } from 'helpers/helpers';
import { IconName } from 'common/enums/enums';
import { postType } from 'common/prop-types/prop-types';
import { IconButton, Image } from 'components/common/common';

import EditPost from 'components/thread/components/edit-post/edit-post';

import styles from './styles.module.scss';
import Message from '../message/message';

const Post = ({ post, onPostLike, getLikedOrDislikedUsers, onPostDislike, onExpandedPostToggle,
  onPostEdit, onDeletePost, sharePost, loginUserId }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const [display, setDisplay] = useState('visible');
  const [edit, setEdit] = useState(false);
  const [likes, setLikes] = useState(false);
  const [likedUsers, setLikedUsers] = useState([]);

  const [dislikes, setDislikes] = useState(false);
  const [dislikedUsers, setDislikedUsers] = useState([]);

  const getLikedUsers = async reaction => {
    const result = await getLikedOrDislikedUsers({ id, reaction });
    if (reaction) {
      setLikedUsers(result.payload);
    } else {
      setDislikedUsers(result.payload);
    }
  };

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleDeletePost = () => {
    setDisplay('none');
    onDeletePost(id);
  };
  const togglePostEdit = () => {
    setDisplay('none');
    setEdit(true);
  };
  const handleCancel = () => {
    setDisplay('visible');
    setEdit(false);
  };

  const handleLikesDisplay = () => {
    getLikedUsers(true);
    setLikes(true);
  };
  const handleLikesHide = () => {
    setLikes(false);
  };

  const handleDislikesHide = () => {
    setDislikes(false);
  };
  const handleLDislikesDisplay = () => {
    getLikedUsers(false);
    setDislikes(true);
  };

  const handlePostEdit = (postId, userId, newBody) => onPostEdit(postId, userId, newBody);

  const userLikes = likedUsers.map(u => (
    <p>
      <IconButton iconName={IconName.LIKE} key={u.id} />
      {u.username}
    </p>
  ));

  const userDislikes = dislikedUsers.map(u => (
    <p>
      <IconButton iconName={IconName.DISLIKE} key={u.id} />
      {u.username}
    </p>
  ));

  return (
    <div className={styles.card}>
      {image && <Image src={image.link} alt="post image" />}
      <div className={styles.content}>
        <div className={styles.meta}>
          <span>{`posted by ${user.username} - ${date}`}</span>
        </div>
        {edit
          ? <EditPost onCancel={handleCancel} onPostEdit={handlePostEdit} userId={loginUserId} post={post} />
          : <p className={styles.description}>{body}</p>}
      </div>
      <div className={styles[display]}>
        <div className={styles.extra}>
          <div className={styles.inline} onMouseEnter={handleLikesDisplay} onMouseLeave={handleLikesHide}>
            <IconButton
              iconName={IconName.THUMBS_UP}
              label={likeCount}
              onClick={handlePostLike}
            />
          </div>
          <div className={styles.inline} onMouseEnter={handleLDislikesDisplay} onMouseLeave={handleDislikesHide}>
            <IconButton
              iconName={IconName.THUMBS_DOWN}
              label={dislikeCount}
              onClick={handlePostDislike}
            />
          </div>
          <IconButton
            iconName={IconName.COMMENT}
            label={commentCount}
            onClick={handleExpandedPostToggle}
          />
          <IconButton
            iconName={IconName.SHARE_ALTERNATE}
            onClick={() => sharePost(id)}
          />
          {post.userId === loginUserId
            ? <IconButton iconName={IconName.EDIT} onClick={togglePostEdit} />
            : null}
          {post.userId === loginUserId
            ? <IconButton iconName={IconName.DELETE} onClick={() => handleDeletePost(id)} />
            : null}
        </div>
        {likes && likedUsers.length > 0
          ? (<Message>{userLikes}</Message>) : null}

        {dislikes && dislikedUsers.length > 0
          ? (<Message>{userDislikes}</Message>) : null}
      </div>
    </div>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  loginUserId: PropTypes.number.isRequired,
  getLikedOrDislikedUsers: PropTypes.func.isRequired,
  onDeletePost: PropTypes.func.isRequired
};

export default Post;
