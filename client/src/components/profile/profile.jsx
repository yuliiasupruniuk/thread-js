import { useSelector, useDispatch } from 'hooks/hooks';
import { Button, Image, Input } from 'components/common/common';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { ImageSize, IconName, ButtonColor, ButtonType } from 'common/enums/enums';
import { useCallback, useState } from 'react';
import { profileActionCreator } from 'store/actions';
import styles from './styles.module.scss';

const Profile = () => {
  const dispatch = useDispatch();

  const { user } = useSelector(state => ({
    user: state.profile.user
  }));

  const handleUsernameChange = useCallback(
    userInfo => dispatch(profileActionCreator.updateUsername(userInfo)),
    [dispatch]
  );

  const handleGetUsers = () => dispatch(profileActionCreator.getUsers());

  const [change, setChange] = useState(false);
  const [checkExist, setCheckExist] = useState(false);
  const [username, setUsername] = useState(user.username);

  const handleChange = () => {
    setUsername(user.username);
    setChange(true);
  };

  const handleChangeUsername = event => {
    setUsername(event.target.value);
    setCheckExist(false);
  };

  const handleSaveChanges = async () => {
    let users = await handleGetUsers();
    users = users.payload;
    const exist = users.filter(u => u.username === username);
    if (exist.length === 0 && username.length !== 0) {
      const { id } = user;
      setChange(false);
      handleUsernameChange({ id, username });
    } else if (exist.length !== 0) {
      setCheckExist(true);
    }
  };

  return (
    <div className={styles.profile}>
      <Image
        alt="profile avatar"
        isCentered
        src={user.image?.link ?? DEFAULT_USER_AVATAR}
        size={ImageSize.MEDIUM}
        isCircular
      />

      {
        !change ? (<Input iconName={IconName.USER} placeholder="Username" value={username} disabled />)
          : (
            <div>
              <Input
                iconName={IconName.USER}
                placeholder="Username"
                value={username}
                onChange={event => handleChangeUsername(event)}
              />
              { checkExist ? <div className={styles.validation}><span>Username already exists</span></div> : null}
              { username.length === 0
                ? <div className={styles.validation}><span>Username can`t be empty</span></div> : null}
            </div>
          )
      }

      <Input
        iconName={IconName.AT}
        placeholder="Email"
        type="email"
        value={user.email}
        disabled
      />

      {
        !change ? (
          <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT} onClick={handleChange}>Change</Button>)
          : (
            <Button
              color={ButtonColor.BLUE}
              type={ButtonType.SUBMIT}
              onClick={handleSaveChanges}
            >
              Save Changes
            </Button>
          )
      }
    </div>
  );
};

export default Profile;
