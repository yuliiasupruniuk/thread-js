import PropTypes from 'prop-types';
import { useState } from 'react';

import { IconName } from 'common/enums/enums';
import { IconButton, TextArea } from 'components/common/common';
import { postType } from 'common/prop-types/prop-types';

import styles from './styles.module.scss';

const EditPost = ({ post, userId, onCancel, onPostEdit }) => {
  const [body, setBody] = useState(post.body);

  const handleChange = event => {
    setBody(event.target.value);
  };

  const handlePostEdit = () => {
    const updatedPost = {
      postId: post.id,
      userId,
      body
    };
    onPostEdit(updatedPost);
  };

  return (
    <div>
      <TextArea
        name="body"
        value={body}
        placeholder="Enter your new thoughts..."
        className={styles.textarea}
        onChange={event => handleChange(event)}
      />
      <div className={styles.btnWrapper}>
        <IconButton iconName={IconName.PEN} onClick={() => handlePostEdit()} />
        <IconButton iconName={IconName.CANCEL} onClick={() => onCancel()} />
      </div>
    </div>
  );
};

EditPost.propTypes = {
  post: postType.isRequired,
  userId: PropTypes.number.isRequired,
  onCancel: PropTypes.func.isRequired,
  onPostEdit: PropTypes.func.isRequired
};

export default EditPost;
