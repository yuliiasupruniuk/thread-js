import { getFromNowTime } from 'helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import { commentType } from 'common/prop-types/prop-types';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { IconButton, TextArea } from 'components/common/common';
import { IconName } from 'common/enums/enums';
import styles from './styles.module.scss';

const Comment = ({ comment: { body, createdAt, user, id }, onCommentEdit, onCommentDelete }) => {
  const { userId } = useSelector(state => ({
    userId: state.profile.user.id
  }));

  const [edit, setEdit] = useState(false);
  const [display, setDisplay] = useState(true);
  const [newBody, setBody] = useState(body);

  const handleChange = event => {
    setBody(event.target.value);
  };

  const toggleCommentEdit = () => {
    setEdit(!edit);
  };

  const handleCommentEdit = () => {
    setEdit(!edit);
    onCommentEdit({ id, newBody });
  };

  const handleCommentDelete = () => {
    setDisplay(false);
    onCommentDelete(id);
  };

  return (
    <div className={display ? styles.comment : styles['display-none']}>
      <div>
        <div>
          <img className={styles.avatar} src={user.image?.link ?? DEFAULT_USER_AVATAR} alt="avatar" />
        </div>
        <div>
          <div>
            <span className={styles.author}>{user.username}</span>
            <span className={styles.date}>{getFromNowTime(createdAt)}</span>
          </div>
          <div className={styles.flexWrapper}>
            {!edit
              ? (
                <p>{newBody}</p>
              ) : null}
            <div>
              {user.id === userId && !edit
                ? <IconButton iconName={IconName.EDIT} onClick={toggleCommentEdit} /> : null}
              {user.id === userId && !edit
                ? <IconButton iconName={IconName.DELETE} onClick={handleCommentDelete} />
                : null}
            </div>
          </div>
          {user.id === userId && edit
            ? (
              <div>
                <TextArea
                  name="body"
                  value={newBody}
                  className={styles.textarea}
                  onChange={event => handleChange(event)}
                />
                <div className={styles.flexWrapper}>
                  <IconButton iconName={IconName.PEN} onClick={handleCommentEdit} />
                  <IconButton iconName={IconName.CANCEL} onClick={toggleCommentEdit} />
                </div>
              </div>
            )
            : null}
        </div>
      </div>
    </div>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  onCommentEdit: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

export default Comment;
