import { HttpMethod, ContentType } from 'common/enums/enums';

class Post {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getAllPosts(filter) {
    return this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getPost(id) {
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  addPost(payload) {
    return this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likePost(postId) {
    return this._http.load(`${this._apiPath}/posts/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: true
      })
    });
  }

  dislikePost(postId) {
    return this._http.load(`${this._apiPath}/posts/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: false
      })
    });
  }

  deletePost(postId) {
    return this._http.load(`${this._apiPath}/posts/${postId}`, {
      method: HttpMethod.DELETE
    });
  }

  editPost(postId, userId, body) {
    return this._http.load(`${this._apiPath}/posts/${postId}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({ userId, body })
    });
  }

  getPostReaction(postId, userId) {
    return this._http.load(`${this._apiPath}/posts/react/${postId}/${userId}`, {
      method: HttpMethod.GET
    });
  }

  getLikedOrDislikedUsers(postId, reaction) {
    return this._http.load(`${this._apiPath}/posts/likes/${postId}`, {
      method: HttpMethod.GET,
      query: { reaction }
    });
  }
}

export { Post };
