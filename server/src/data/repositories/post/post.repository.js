import { Abstract } from '../abstract/abstract.repository';
import { getCommentsCountQuery, getReactionsQuery, getWhereUserIdQuery } from './helpers';

class Post extends Abstract {
  constructor({ postModel }) {
    super(postModel);
  }

  getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      hideId
    } = filter;

    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where(getWhereUserIdQuery(userId))
      .skipUndefined()
      .whereNot('posts.user_id', hideId)
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getLikedByUserPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId, hideId
    } = filter;

    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .join('post_reactions', 'post_reactions.post_id', '=', 'posts.id')
      .where({ isLike: true })
      .andWhere('post_reactions.user_id', '=', userId)
      .skipUndefined()
      .whereNot('posts.user_id', hideId)
      .withGraphFetched('[image, user.image]')
      .orderBy('createdAt', 'desc')
      .offset(offset)
      .limit(limit);
  }

  getPostById(id) {
    return this.model.query()
      .select(
        'posts.*',
        getCommentsCountQuery(this.model),
        getReactionsQuery(this.model)(true),
        getReactionsQuery(this.model)(false)
      )
      .where({ id })
      .withGraphFetched('[comments.user.image, user.image, image]')
      .first();
  }

  updatePost(id, userId, body) {
    return this.model.query()
      .update({ userId, body })
      .where({ id });
  }

  deletePostbyId(id) {
    return this.deleteById(id);
  }
}

export { Post };
