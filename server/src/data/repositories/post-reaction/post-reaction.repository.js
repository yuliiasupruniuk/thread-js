import { Abstract } from '../abstract/abstract.repository';

class PostReaction extends Abstract {
  constructor({ postReactionModel }) {
    super(postReactionModel);
  }

  getReactionUsers(postId, reaction) {
    return this.model.query()
      .select('users.id', 'users.username')
      .join('users', 'users.id', '=', 'post_reactions.user_id')
      .where({ postId })
      .andWhere({ isLike: reaction });
  }

  getPostReaction(userId, postId) {
    return this.model.query()
      .select()
      .where({ userId })
      .andWhere({ postId })
      .withGraphFetched('[post]')
      .first();
  }
}

export { PostReaction };
