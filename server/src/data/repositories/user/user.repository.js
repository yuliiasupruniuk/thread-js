import { Abstract } from '../abstract/abstract.repository';

class User extends Abstract {
  constructor({ userModel }) {
    super(userModel);
  }

  getUsers() {
    return this.model.query().select('users.id', 'users.username', 'users.email');
  }

  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.query().select().where({ email }).first();
  }

  getByUsername(username) {
    return this.model.query().select().where({ username }).first();
  }

  setUsername(id, username) {
    return this.updateById(id, { username });
  }

  getUserById(id) {
    return this.model.query()
      .select('id', 'createdAt', 'email', 'updatedAt', 'username')
      .where({ id })
      .withGraphFetched('[image]')
      .first();
  }
}

export { User };
