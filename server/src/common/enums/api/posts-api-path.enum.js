const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  $LIKES: '/likes/:id',
  REACT: '/react',
  $CHECK: '/react/:postid/:userid'
};

export { PostsApiPath };
