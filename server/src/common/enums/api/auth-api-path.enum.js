const AuthApiPath = {
  LOGIN: '/login',
  REGISTER: '/register',
  USER: '/user',
  ALL_USERS: '/users',
  $USER_ID: '/users/:id'
};

export { AuthApiPath };
