class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    const { liked } = filter;
    if (liked === 'true') {
      return this._postRepository.getLikedByUserPosts(filter);
    }
    return this._postRepository.getPosts(filter);
  }

  getReactionUsers(id, query) {
    const { reaction } = query;
    return this._postReactionRepository.getReactionUsers(id, reaction);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  deletePostbyId(id) {
    return this._postRepository.deletePostbyId(id);
  }

  updatePost(id, data) {
    const { userId, body } = data;
    return this._postRepository.updatePost(id, userId, body);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  async getPostReaction(postId, userId) {
    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    return !reaction ? {} : reaction;
  }

  async setReaction(userId, { postId, isLike = true }) {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
      ? this._postReactionRepository.deleteById(react.id)
      : this._postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }
}

export { Post };
