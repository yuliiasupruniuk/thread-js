class Comment {
  constructor({ commentRepository }) {
    this._commentRepository = commentRepository;
  }

  create(userId, comment) {
    return this._commentRepository.create({
      ...comment,
      userId
    });
  }

  getCommentById(id) {
    return this._commentRepository.getCommentById(id);
  }

  updateComment(id, data) {
    const { body } = data;
    return this._commentRepository.updateComment(id, body);
  }

  deleteCommentById(id) {
    return this._commentRepository.deleteCommentById(id);
  }
}

export { Comment };
